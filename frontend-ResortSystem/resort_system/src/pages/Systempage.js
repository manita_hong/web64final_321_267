import Header2 from '../componants/Header2';
import { Box, AppBar, Typography, Toolbar, Card,Paper} from '@mui/material';
import CardContent from '@mui/material/CardContent';
import * as React from 'react';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { useNavigate } from "react-router-dom";
import { CardActionArea } from '@mui/material';
import CardMedia from '@mui/material/CardMedia';
import { styled } from '@mui/material/styles';

import TextField from '@mui/material/TextField';

import { useTheme } from '@mui/material/styles';
import Homepage from '../pages/Homepage.css';
import Rating from '@mui/material/Rating';

import IconButton from '@mui/material/IconButton';
import SkipPreviousIcon from '@mui/icons-material/SkipPrevious';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import SkipNextIcon from '@mui/icons-material/SkipNext';



import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

function Systempage(){

    const [resort, setResort] = React.useState('');
  
    const handleChange = (event) => {
      setResort(event.target.value);
    };

    let navigate_home2 = useNavigate(); 
    const routeChange_home2 = () =>{ 
      let path_home2 = `/home2`; 
      navigate_home2(path_home2);
    }
    let navigate_process = useNavigate(); 
    const routeChange_process = () =>{ 
      let path_process = `/process`; 
      navigate_process(path_process);
    }

    return(
        <div>
            <Header2/>
            <Box sx={{  pl: 6,pr: 6, pt: 2,}}>
                    <Box sx={{pb: 5, pt: 2 ,pl: 6,pr: 6, textAlign: 'center' , fontFamily: 'RSU', fontSize: 'h2.fontSize', m: 1, }}>
                            กรุณากรอกข้อมูลการจอง
                    </Box>
                   
                    <Box sx={{ minWidth: 120 }}>
                        <FormControl sx={{ minWidth: 350 }}>
                            <InputLabel id="demo-simple-select-label">ชื่อรีสอร์ท</InputLabel>
                            <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={resort}
                            label="ชื่อรีสอร์ท"
                            onChange={handleChange}
                            >
                            <MenuItem value={10}>MayMizzy Resort หาดป่าตอง</MenuItem>
                            <MenuItem value={20}>Honggy Resort หาดอ่าวนาง</MenuItem>
                            <MenuItem value={30}>The Rose Resort เกาะพีพี</MenuItem>
                            <MenuItem value={40}>Richie and Sky Resort เกาะหลีเป๊ะ</MenuItem>
                            <MenuItem value={50}>Anantaya Resort เกาะลิป</MenuItem>
                            <MenuItem value={60}>Woof Resort เกาะเต่า</MenuItem>
                            <MenuItem value={70}>Punya Resort เกาะเสม็ด</MenuItem>
                            <MenuItem value={80}>ฺBodywhite Resort เกาะพิทักษ์</MenuItem>
                            <MenuItem value={90}>Yaya Resort เกาะสีชัง</MenuItem>
                            <MenuItem value={100}>Loveing You Resort เกาะลันตา</MenuItem>

                            </Select>
                        </FormControl>

                        <Box sx={{pb:4, pt: 4,}}> 
                            <TextField label="จำนวนคืน" variant="outlined" sx={{ minWidth: 350 }}/>
                        </Box>  
                        <TextField  label="พักตั้งแต่วันที่-ถึงวันที่" variant="outlined" sx={{ minWidth: 350 }}/>
                        
                        <Box sx={{pb: 9, pt: 4,}}>
                          <Button variant="contained" size="large" onClick={routeChange_process} >
                            ยืนยันการจอง
                          </Button> &nbsp;
                          <Button variant="outlined" size="large" onClick={routeChange_home2} >
                            ยกเลิก
                          </Button>
                        </Box>
                        
                     
                </Box>
            </Box>

              
        </div>
    );

}

export default  Systempage;