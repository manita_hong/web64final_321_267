import Header2 from '../componants/Header2';
import { Box, AppBar, Typography, Toolbar, Card,Paper} from '@mui/material';
import CardContent from '@mui/material/CardContent';
import * as React from 'react';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { useNavigate } from "react-router-dom";
import { CardActionArea } from '@mui/material';
import CardMedia from '@mui/material/CardMedia';
import { styled } from '@mui/material/styles';

import TextField from '@mui/material/TextField';

import { useTheme } from '@mui/material/styles';
import Homepage from '../pages/Homepage.css';
import Rating from '@mui/material/Rating';

import IconButton from '@mui/material/IconButton';
import SkipPreviousIcon from '@mui/icons-material/SkipPrevious';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import SkipNextIcon from '@mui/icons-material/SkipNext';



import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

function Processpage(){

    
  let navigate_home2 = useNavigate(); 
  const routeChange_home2 = () =>{ 
    let path_home2 = `/home2`; 
    navigate_home2(path_home2);
  }

    return(
        <div>
            <Header2/>
            <Box sx={{  pl: 6,pr: 6, pb: 2, pt: 2,  }}>
              <img id='img_1' src="/pic/check.png" width="300 px" height="300 px"/>
                  <Box>
                    <Typography gutterBottom variant="h3" component="div" fontFamily= 'RSU'>
                        ทำการจองเสร็จสิ้น
                    </Typography>
                  </Box>
                  <Box sx={{ pb: 3, pt: 2, }}>
                    <Button variant="contained" size="large" onClick={routeChange_home2}>
                        กลับเข้าสู่หน้าหลัก
                    </Button>
                  </Box>
            </Box>
        </div>
    );

}

export default  Processpage;