import { Box, AppBar, Typography, Toolbar, Card,Paper} from '@mui/material';
import CardContent from '@mui/material/CardContent';
import * as React from 'react';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { useNavigate } from "react-router-dom";
import { CardActionArea } from '@mui/material';
import CardMedia from '@mui/material/CardMedia';
import { styled } from '@mui/material/styles';

import Header from '../componants/Header';


const Item = styled(Paper)(({ theme }) => ({
  padding: theme.spacing(0),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

function Homepage() {

  let navigate = useNavigate(); 
  const routeChange_login = () =>{ 
    let path_login = `/login`; 
    navigate(path_login);
  }
  
  let navigate_regis = useNavigate(); 
  const routeChange_regis = () =>{ 
    let path_regis = `/register`; 
    navigate_regis(path_regis);
  }
  
   

    return (
     
        
        <div>
            
         
              <Header/>


              <Box sx={{  pl: 6,pr: 6, pb: 2, pt: 2,  backgroundColor: 'secondary.light',}}>
                  <Box sx={{pb: 1, pt: 1,pl: 6,pr: 6, textAlign: 'center' , fontFamily: 'Brush Script MT', fontSize: 'h1.fontSize', m: 1,}}>
                    Welcome 
                  </Box>

                  
                    <Grid  container spacing={2}  >
            
                          <Grid item xs={8} lg={3} >
                            <Item>
                                <Card >
                                    <CardActionArea>
                                        <CardMedia
                                          component="img"
                                          height="250"
                                          image="/pic/p1.png"
                                          alt="green iguana"
                                        />
                                      <CardContent>
                                        <Typography gutterBottom variant="h5" component="div" fontFamily= 'Bebas Neue'>
                                              MayMizzy Resort
                                          </Typography>
                                          <Typography variant="body1" color="text.secondary" fontFamily= 'RSU'>
                                              ราคาเริ่มต้น 1,500 บาท / คืน
                                        </Typography>
                                      </CardContent>
                                    </CardActionArea>
                                </Card>
                            </Item>
                          </Grid>

                          
                          
                          <Grid item xs={8} lg={3} >
                            <Item>
                                <Card >
                                    <CardActionArea>
                                      <CardMedia
                                          component="img"
                                          height="250"
                                          image="/pic/p2.png"
                                          alt="green iguana"
                                        />
                                        <CardContent>
                                              <Typography gutterBottom variant="h5" component="div" fontFamily= 'Bebas Neue'>
                                                  Honggy Resort
                                              </Typography>
                                              <Typography variant="body1" color="text.secondary" fontFamily= 'RSU'>
                                                  ราคาเริ่มต้น 1,000 บาท / คืน
                                              </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>
                              </Item>
                          </Grid>



                          <Grid item xs={8} lg={3}>
                            <Item>
                                <Card >
                                    <CardActionArea >
                                        <CardMedia
                                          component="img"
                                          height="250"
                                          image="/pic/p3.png"
                                          alt="green iguana"
                                        />
                                        <CardContent>
                                            <Typography gutterBottom variant="h5" component="div" fontFamily= 'Bebas Neue'>
                                                The Rose Resort
                                            </Typography>
                                            <Typography variant="body1" color="text.secondary" fontFamily= 'RSU'>
                                                ราคาเริ่มต้น 800 บาท / คืน
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                </Card>
                              </Item>
                          </Grid>

                      
                      
                          <Grid item xs={8} lg={3}>
                              <Item>
                                  <Card >
                                      <CardActionArea >
                                          <CardMedia
                                            component="img"
                                            height="250"
                                            image="/pic/p4.png"
                                            alt="green iguana"
                                          />
                              
                                          <CardContent>
                                              <Typography gutterBottom variant="h5" component="div" fontFamily= 'Bebas Neue'>
                                                Anantaya Resort
                                              </Typography>
                                              <Typography variant="body1" color="text.secondary" fontFamily= 'RSU'>
                                                ราคาเริ่มต้น 1,500 บาท / คืน
                                              </Typography>
                                          </CardContent>
                                      </CardActionArea>
                                  </Card>
                                </Item>
                          </Grid>
                    
      
                    </Grid>
        
        
                    <Box sx={{pb: 4, pt: 5, backgroundColor: 'secondary.light',}}>
                          <Button variant="contained" size="large" onClick={routeChange_login}>
                            เข้าสู่ระบบเพื่อจองทรีสอร์ท
                          </Button> &nbsp;
                          <Button variant="outlined" size="large" onClick={routeChange_regis} >
                            สมัครสมาชิก
                          </Button>
                    </Box>
             </Box>
          
        </div>
    );
  };

  export default Homepage;



 