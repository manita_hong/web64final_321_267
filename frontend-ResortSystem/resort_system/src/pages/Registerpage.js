import * as React from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Header from '../componants/Header';
import { useNavigate } from "react-router-dom";


function Registerpage() {
  
  let navigate_home2 = useNavigate(); 
  const routeChange_home2 = () =>{ 
    let path_home2 = `/home2`; 
    navigate_home2(path_home2);
  }

  return (

    
    <ThemeProvider >
      <Header/>
      <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >

              <img id='img_1' src="/airplane-around-earth.png" width="50 px" height="50 px"/>
              <Typography component="h1" variant="h3" fontFamily= 'RSU'>
                สมัครสมาชิก
              </Typography>
              
              <Box component="form"  sx={{ mt: 3 }}>
                <Grid container spacing={2}>
                  
                  <Grid item xs={12} sm={6}>
                    <TextField
                      autoComplete="given-name"
                      name="firstName"
                      required
                      fullWidth
                      id="firstName"
                      label="ชื่อ"
                      autoFocus
                    />
                  </Grid>

                  <Grid item xs={12} sm={6}>
                    <TextField
                      required
                      fullWidth
                      id="lastName"
                      label="นามสกุล"
                      name="lastName"
                      autoComplete="family-name"
                    />
                  </Grid>
                  
                  <Grid item xs={12}>
                    <TextField
                      required
                      fullWidth
                      id="email"
                      label="ชื่อผู้ใช้"
                      name="email"
                      autoComplete="email"
                    />
                  </Grid>
                  
                  <Grid item xs={12}>
                    <TextField
                      required
                      fullWidth
                      name="password"
                      label="รหัสผ่าน"
                      type="password"
                      id="password"
                      autoComplete="new-password"
                    />
                  </Grid>
                </Grid> 
            
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                  onClick={routeChange_home2}>
                 สมัครสมาชิก
                </Button>
            
                <Grid container justifyContent="flex-end">
                    <Grid item>
                      <Box sx={{   pb: 8,  }}/>
                    </Grid>
                </Grid>
            </Box>
         </Box>
       
      </Container>
    </ThemeProvider>
  );
}
export default Registerpage;