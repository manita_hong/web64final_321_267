import * as React from 'react';

import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';

import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import { useNavigate } from "react-router-dom";
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Header from '../componants/Header';
 

  
 function Login() {
   
  let navigate_home2 = useNavigate(); 
  const routeChange_home2 = () =>{ 
    let path_home2 = `/home2`; 
    navigate_home2(path_home2);
  }
    return (
            <ThemeProvider>
               <Header/>
                <Container component="main" maxWidth="xs" >
                      <CssBaseline />
                      <Box
                        sx={{
                          marginTop: 8,
                          display: 'flex',
                          flexDirection: 'column',
                          alignItems: 'center',
                          
                        }}
                      >
                    
                            <img id='img_1' src="/airplane-around-earth.png" width="50 px" height="50 px"/>
                            <Typography component="h1" variant="h3" fontFamily= 'RSU'>
                              ยินดีต้อนรับเข้าสู่
                            </Typography>
                            <Typography component="h1" variant="h4" fontFamily= 'RSU'>
                            ระบบจองรีสอร์ท 
                            </Typography>
                        
                  
                            <Box component="form"  noValidate sx={{ mt: 1 ,pb: 7}}>
                                    <TextField
                                      margin="normal"
                                      required
                                      fullWidth
                                      id="user"
                                      type="username"
                                      label="ชื่อผู้ใช้"
                                      autoComplete="user"
                                      autoFocus
                                    />
                          
                                  <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    name="password"
                                    id="password"
                                    type="password"
                                    label="รหัสผ่าน"
                                    autoComplete="current-password"
                                  />
                          
                                  <Button
                                    fullWidth
                                    variant="contained"
                                    sx={{ mt: 3, mb: 2 }}
                                    onClick={routeChange_home2}
                                  >
                                    เข้าสู่ระบบ
                                  </Button>
                          
                                  <Grid container>
                                      <Grid item xs/>
                                          <Grid item>
                                            <Link href="/register" variant="body2">
                                                {"คุณยังไม่เป็นสมาชิกในระบบ ใช่หรือไม่"}
                                            </Link>
                                          </Grid>
                                  </Grid>
                            </Box>
                      </Box>
                </Container>
            </ThemeProvider>
    );
  }
  export default Login;