import { Box, AppBar, Typography, Toolbar, Card,Paper} from '@mui/material';
import CardContent from '@mui/material/CardContent';
import * as React from 'react';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { useNavigate } from "react-router-dom";
import { CardActionArea } from '@mui/material';
import CardMedia from '@mui/material/CardMedia';
import { styled } from '@mui/material/styles';

import Header2 from '../componants/Header2';

import { useTheme } from '@mui/material/styles';
import Homepage from '../pages/Homepage.css';
import Rating from '@mui/material/Rating';

import IconButton from '@mui/material/IconButton';
import SkipPreviousIcon from '@mui/icons-material/SkipPrevious';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import SkipNextIcon from '@mui/icons-material/SkipNext';



function Homepage2() {

  let navigate_system = useNavigate(); 
  const routeChange_system = () =>{ 
    let path_system = `/system`; 
    navigate_system(path_system);
  }

  const theme = useTheme();
   
  const [value1, setValue1] = React.useState(3);
  const [value2, setValue2] = React.useState(4);
  const [value3, setValue3] = React.useState(5);
  const [value4, setValue4] = React.useState(3);
  const [value5, setValue5] = React.useState(3);
  const [value6, setValue6] = React.useState(5);
  const [value7, setValue7] = React.useState(4);
  const [value8, setValue8] = React.useState(4);
  const [value9, setValue9] = React.useState(5);
  const [value10, setValue10] = React.useState(3);  
  return (
     
        
        <div>
            
         
              
                <Header2/>

              <Box sx={{  pl: 6,pr: 6, pb: 2, pt: 2,  backgroundColor: 'secondary.light',}}>
                  <Box sx={{pb: 7, pt: 1,pl: 6,pr: 6, textAlign: 'center' , fontFamily: 'Brush Script MT', fontSize: 'h1.fontSize', m: 1,}}>
                    Welcome to ResortSystem
                  </Box>
                  

                        <Box sx={{ pb: 9,backgroundColor: 'secondary.light',}}>
                              <Grid id="g1" container spacing={2} >

                                  <CardMedia
                                    component="img"
                                    sx={{ width: 400}}
                                    image="/pic/p1.png"
                                    alt="Live from space album cover"
                                  />

                                  <Card sx={{ display: 'flex' }}>
                                  <Box sx={{ display: 'flex', flexDirection: 'column' ,pt:2.5,}}>
                                        <CardContent sx={{ width: 400 }}>
                                            <Typography component="div" variant="h4" fontFamily= 'Bebas Neue'>
                                              MayMizzy  Resort
                                            </Typography>
                                            <Typography variant="subtitle1" color="text.secondary" component="div" textAlign='left' sx={{  pl: 6,pr: 6, pb: 2, pt: 3,}}>
                                               สถานที่ : หาดป่าตอง | 
                                               ราคา : 1,500 บาท/คืน
                                            </Typography>
                                            <Box sx={{ display: 'flex', alignItems: 'center', pl: 15, pb: 3 ,}}>
                                              
                                              <Typography component="legend">Ratting</Typography>
                                              <Rating name="read-only" value={value1} readOnly />
                                            </Box>
                                        </CardContent>
                              
                                        
                                        <Box>
                                          <Button 
                                          variant="contained" 
                                          color="primary"   
                                          style={{ marginLeft: "auto" }} 
                                          onClick={routeChange_system}
                                          >
                                                จองรีสอร์ทที่นี่
                                          </Button>
                                          </Box>
                                  </Box>
                                  </Card>

                            </Grid>
                      </Box>

                          
                      <Box sx={{ pb: 9,backgroundColor: 'secondary.light',}}>       
                                  <Grid id="g1" container spacing={2}  >

                                    <CardMedia
                                      component="img"
                                      sx={{ width: 400}}
                                      image="/pic/p2.png"
                                      alt="Live from space album cover"
                                    />

                                    <Card sx={{ display: 'flex' }}>
                                        <Box sx={{ display: 'flex', flexDirection: 'column', pt:2.5,}}>
                                            <CardContent sx={{ width: 400 }}>
                                                <Typography component="div" variant="h4" fontFamily= 'Bebas Neue' >
                                                    Honggy  Resort
                                                </Typography>
                                                <Typography variant="subtitle1" color="text.secondary" component="div" textAlign='left' sx={{  pl: 6,pr: 6, pb: 2, pt: 3,}}>
                                                สถานที่ : หาดอ่าวนาง | 
                                               ราคา : 1,000 บาท/คืน
                                                </Typography>
                                                <Box sx={{ display: 'flex', alignItems: 'center', pl: 15, pb: 3 ,}}>
                                              
                                              <Typography component="legend">Ratting</Typography>
                                              <Rating name="read-only" value={value2} readOnly />
                                            </Box>
                                            </CardContent>
                                              
                                              <Box>
                                          <Button 
                                          variant="contained" 
                                          color="primary"   
                                          style={{ marginLeft: "auto" }} 
                                          onClick={routeChange_system}
                                          >
                                                จองรีสอร์ทที่นี่
                                          </Button>
                                          </Box>
                                        </Box>
                                    </Card>
                                  </Grid>
                      </Box>

                      <Box sx={{ pb: 9,backgroundColor: 'secondary.light',}}>       
                                  <Grid id="g1" container spacing={2}  >

                                    <CardMedia
                                      component="img"
                                      sx={{ width: 400}}
                                      image="/pic/p3.png"
                                      alt="Live from space album cover"
                                    />

                                    <Card sx={{ display: 'flex' }}>
                                        <Box sx={{ display: 'flex', flexDirection: 'column' , pt:2.5,}}>
                                            <CardContent sx={{ width: 400 }}>
                                                <Typography component="div" variant="h4" fontFamily= 'Bebas Neue'>
                                                    The Rose  Resort
                                                </Typography>
                                                <Typography variant="subtitle1" color="text.secondary" component="div" textAlign='left' sx={{  pl: 6,pr: 6, pb: 2, pt: 3,}}>
                                                สถานที่ : เกาะพีพี | 
                                               ราคา : 800 บาท/คืน
                                                </Typography>
                                                <Box sx={{ display: 'flex', alignItems: 'center', pl: 15, pb: 3 ,}}>
                                              
                                              <Typography component="legend">Ratting</Typography>
                                              <Rating name="read-only" value={value3} readOnly />
                                            </Box>
                                            </CardContent>
                                             
                                              <Box>
                                          <Button 
                                          variant="contained" 
                                          color="primary"   
                                          style={{ marginLeft: "auto" }} 
                                          onClick={routeChange_system}
                                          >
                                                จองรีสอร์ทที่นี่
                                          </Button>
                                          </Box>
                                        </Box>
                                    </Card>
                                  </Grid>
                      </Box>


                      <Box sx={{ pb: 9,backgroundColor: 'secondary.light',}}>       
                                  <Grid id="g1" container spacing={2}  >

                                    <CardMedia
                                      component="img"
                                      sx={{ width: 400}}
                                      image="/pic/p4.png"
                                      alt="Live from space album cover"
                                    />

                                    <Card sx={{ display: 'flex' }}>
                                        <Box sx={{ display: 'flex', flexDirection: 'column' , pt:2.5,}}>
                                            <CardContent sx={{ width: 400 }}>
                                                <Typography component="div" variant="h4" fontFamily= 'Bebas Neue'>
                                                    Richie and Sky Resort
                                                </Typography>
                                                <Typography variant="subtitle1" color="text.secondary" component="div" textAlign='left' sx={{  pl: 6,pr: 6, pb: 2, pt: 3,}}>
                                                สถานที่ : เกาะหลีเป๊ะ | 
                                               ราคา : 1,500 บาท/คืน
                                                </Typography>
                                                <Box sx={{ display: 'flex', alignItems: 'center', pl: 15, pb: 3 ,}}>
                                              
                                              <Typography component="legend">Ratting</Typography>
                                              <Rating name="read-only" value={value4} readOnly />
                                            </Box>
                                            </CardContent>
                                              
                                              <Box>
                                          <Button 
                                          variant="contained" 
                                          color="primary"   
                                          style={{ marginLeft: "auto" }} 
                                          onClick={routeChange_system}
                                          >
                                                จองรีสอร์ทที่นี่
                                          </Button>
                                          </Box>
                                        </Box>
                                    </Card>
                                  </Grid>
                      </Box>

                      <Box sx={{ pb: 9,backgroundColor: 'secondary.light',}}>       
                                  <Grid id="g1" container spacing={2}  >

                                    <CardMedia
                                      component="img"
                                      sx={{ width: 400}}
                                      image="/pic/p5.png"
                                      alt="Live from space album cover"
                                    />

                                    <Card sx={{ display: 'flex' }}>
                                        <Box sx={{ display: 'flex', flexDirection: 'column' , pt:2.5, }}>
                                            <CardContent sx={{ width: 400 }}>
                                                <Typography component="div" variant="h4" fontFamily= 'Bebas Neue'>
                                                    Anantaya Resort
                                                </Typography>
                                                <Typography variant="subtitle1" color="text.secondary" component="div" textAlign='left' sx={{  pl: 6,pr: 6, pb: 2, pt: 3,}}>
                                                สถานที่ : เกาะลิป | 
                                               ราคา : 1,200 บาท/คืน
                                                </Typography>
                                                <Box sx={{ display: 'flex', alignItems: 'center', pl: 15, pb: 3 ,}}>
                                              
                                              <Typography component="legend">Ratting</Typography>
                                              <Rating name="read-only" value={value5} readOnly />
                                            </Box>
                                            </CardContent>
                                              
                                              <Box>
                                          <Button 
                                          variant="contained" 
                                          color="primary"   
                                          style={{ marginLeft: "auto" }} 
                                          onClick={routeChange_system}
                                          >
                                                จองรีสอร์ทที่นี่
                                          </Button>
                                          </Box>
                                        </Box>
                                    </Card>
                                  </Grid>
                      </Box>

                      <Box sx={{ pb: 9,backgroundColor: 'secondary.light',}}>       
                                  <Grid id="g1" container spacing={2}  >

                                    <CardMedia
                                      component="img"
                                      sx={{ width: 400}}
                                      image="/pic/p6.png"
                                      alt="Live from space album cover"
                                    />

                                    <Card sx={{ display: 'flex' }}>
                                        <Box sx={{ display: 'flex', flexDirection: 'column' , pt:2.5,}}>
                                            <CardContent sx={{ width: 400 }}>
                                                <Typography component="div" variant="h4" fontFamily= 'Bebas Neue'>
                                                    Woof Resort
                                                </Typography>
                                                <Typography variant="subtitle1" color="text.secondary" component="div" textAlign='left' sx={{  pl: 6,pr: 6, pb: 2, pt: 3,}}>
                                                สถานที่ : เกาะเต่า | 
                                               ราคา : 1,500 บาท/คืน
                                                </Typography>
                                                <Box sx={{ display: 'flex', alignItems: 'center', pl: 15, pb: 3 ,}}>
                                              
                                              <Typography component="legend">Ratting</Typography>
                                              <Rating name="read-only" value={value6} readOnly />
                                            </Box>
                                            </CardContent>
                                              
                                              <Box>
                                          <Button 
                                          variant="contained" 
                                          color="primary"   
                                          style={{ marginLeft: "auto" }} 
                                          onClick={routeChange_system}
                                          >
                                                จองรีสอร์ทที่นี่
                                          </Button>
                                          </Box>
                                        </Box>
                                    </Card>
                                  </Grid>
                      </Box>

                      <Box sx={{ pb: 9,backgroundColor: 'secondary.light',}}>       
                                  <Grid id="g1" container spacing={2}  >

                                    <CardMedia
                                      component="img"
                                      sx={{ width: 400}}
                                      image="/pic/p7.png"
                                      alt="Live from space album cover"
                                    />

                                    <Card sx={{ display: 'flex' }}>
                                        <Box sx={{ display: 'flex', flexDirection: 'column' , pt:2.5, }}>
                                            <CardContent sx={{ width: 400 }}>
                                                <Typography component="div" variant="h4" fontFamily= 'Bebas Neue'>
                                                    Punya  Resort
                                                </Typography>
                                                <Typography variant="subtitle1" color="text.secondary" component="div" textAlign='left' sx={{  pl: 6,pr: 6, pb: 2, pt: 3,}}>
                                                สถานที่ : เกาะเสม็ด | 
                                               ราคา : 850 บาท/คืน
                                                </Typography>
                                                <Box sx={{ display: 'flex', alignItems: 'center', pl: 15, pb: 3 ,}}>
                                              
                                              <Typography component="legend">Ratting</Typography>
                                              <Rating name="read-only" value={value7} readOnly />
                                            </Box>
                                            </CardContent>
                                             
                                              <Box>
                                          <Button 
                                          variant="contained" 
                                          color="primary"   
                                          style={{ marginLeft: "auto" }} 
                                          onClick={routeChange_system}
                                          >
                                                จองรีสอร์ทที่นี่
                                          </Button>
                                          </Box>
                                        </Box>
                                    </Card>
                                  </Grid>
                      </Box>

                      <Box sx={{ pb: 9,backgroundColor: 'secondary.light',}}>       
                                  <Grid id="g1" container spacing={2}  >

                                    <CardMedia
                                      component="img"
                                      sx={{ width: 400}}
                                      image="/pic/p8.png"
                                      alt="Live from space album cover"
                                    />

                                    <Card sx={{ display: 'flex' }}>
                                        <Box sx={{ display: 'flex', flexDirection: 'column' , pt:2.5,}}>
                                            <CardContent sx={{ width: 400 }}>
                                                <Typography component="div" variant="h4" fontFamily= 'Bebas Neue'>
                                                    BodyWhite  Resort
                                                </Typography>
                                                <Typography variant="subtitle1" color="text.secondary" component="div" textAlign='left' sx={{  pl: 6,pr: 6, pb: 2, pt: 3,}}>
                                                สถานที่ : เกาะพิทักษ์ | 
                                               ราคา : 1,000 บาท/คืน
                                                </Typography>
                                                <Box sx={{ display: 'flex', alignItems: 'center', pl: 15, pb: 3 ,}}>
                                              
                                              <Typography component="legend">Ratting</Typography>
                                              <Rating name="read-only" value={value8} readOnly />
                                            </Box>
                                            </CardContent>
                                              
                                              <Box>
                                          <Button 
                                          variant="contained" 
                                          color="primary"   
                                          style={{ marginLeft: "auto" }} 
                                          onClick={routeChange_system}
                                          >
                                                จองรีสอร์ทที่นี่
                                          </Button>
                                          </Box>
                                              
                                        </Box>
                                    </Card>
                                  </Grid>
                      </Box>

                      <Box sx={{ pb: 9,backgroundColor: 'secondary.light',}}>       
                                  <Grid id="g1" container spacing={2}  >

                                    <CardMedia
                                      component="img"
                                      sx={{ width: 400}}
                                      image="/pic/p9.png"
                                      alt="Live from space album cover"
                                    />

                                    <Card sx={{ display: 'flex' }}>
                                        <Box sx={{ display: 'flex', flexDirection: 'column' , pt:2.5,}}>
                                            <CardContent sx={{ width: 400 }}>
                                                <Typography component="div" variant="h4" fontFamily= 'Bebas Neue'>
                                                    YaYa Resort
                                                </Typography>
                                                <Typography variant="subtitle1" color="text.secondary" component="div" textAlign='left' sx={{  pl: 6,pr: 6, pb: 2, pt: 3,}}>
                                                สถานที่ : เกาะสีชัง | 
                                               ราคา : 1,500 บาท/คืน
                                                </Typography>
                                                <Box sx={{ display: 'flex', alignItems: 'center', pl: 15, pb: 3 ,}}>
                                              
                                              <Typography component="legend">Ratting</Typography>
                                              <Rating name="read-only" value={value9} readOnly />
                                            </Box>
                                            </CardContent>
                                              
                                              <Box>
                                          <Button 
                                          variant="contained" 
                                          color="primary"   
                                          style={{ marginLeft: "auto" }} 
                                          onClick={routeChange_system}
                                          >
                                                จองรีสอร์ทที่นี่
                                          </Button>
                                          </Box>
                                        </Box>
                                    </Card>
                                  </Grid>
                      </Box>

                      <Box sx={{ pb: 9,backgroundColor: 'secondary.light',}}>       
                                  <Grid id="g1" container spacing={2}  >

                                    <CardMedia
                                      component="img"
                                      sx={{ width: 400}}
                                      image="/pic/p10.png"
                                      alt="Live from space album cover"
                                    />

                                    <Card sx={{ display: 'flex' }}>
                                        <Box sx={{ display: 'flex', flexDirection: 'column' , pt:2.5,}}>
                                            <CardContent sx={{ width: 400 }}>
                                                <Typography component="div" variant="h4" fontFamily= 'Bebas Neue'>
                                                    Loveing You  Resort
                                                </Typography>
                                                <Typography variant="subtitle1" color="text.secondary" component="div" textAlign='left' sx={{  pl: 6,pr: 6, pb: 2, pt: 3,}}>
                                                สถานที่ : เกาะลันตา | 
                                               ราคา : 1,200 บาท/คืน
                                                </Typography>
                                                <Box sx={{ display: 'flex', alignItems: 'center', pl: 15, pb: 3 ,}}>
                                              
                                              <Typography component="legend">Ratting</Typography>
                                              <Rating name="read-only" value={value10} readOnly />
                                            </Box>
                                            </CardContent>
                                              
                                              <Box>
                                          <Button 
                                          variant="contained" 
                                          color="primary"   
                                          style={{ marginLeft: "auto" }} 
                                          onClick={routeChange_system}
                                          >
                                                จองรีสอร์ทที่นี่
                                          </Button>
                                          </Box>
                                        </Box>
                                    </Card>
                                  </Grid>
                      </Box>










              </Box>  
 

          
        </div>

);
};

export default Homepage2;



 