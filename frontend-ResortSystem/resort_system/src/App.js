import logo from './logo.svg';
import './App.css';

import Header from './componants/Header';
import Footer from './componants/Footer';
import {Routes,Route} from "react-router-dom";
import Homepage from './pages/Homepage';
import Loginpage from './pages/Loginpage';
import Registerpage from './pages/Registerpage';
import Homepage2 from './pages/Homepage2';
import Systempage from './pages/Systempage';
import Processpage from './pages/Processpage';

function App() {
  return (
    <div className="App">
      
     

        <Routes>
            <Route path="/" element={
            <Homepage/>
            }/>
            <Route path="login" element={
            <Loginpage/>
            }/>
            <Route path="register" element={
            <Registerpage/>
            }/>
            <Route path="home2" element={
            <Homepage2/>
            }/>
            <Route path="system" element={
            <Systempage/>
            }/>
            <Route path="process" element={
            <Processpage/>
            }/>
        </Routes>
      <Footer/>
    </div>
    
  );
}

export default App;
