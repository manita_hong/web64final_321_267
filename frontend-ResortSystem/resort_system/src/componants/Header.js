import './Header.css';
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Tooltip from '@mui/material/Tooltip';
import MenuItem from '@mui/material/MenuItem';
import {createTheme} from '@mui/material';
import Link from '@mui/material/Link';
import ButtonGroup from '@mui/material/ButtonGroup';
import { useNavigate } from "react-router-dom";


function Header() {

    let navigate = useNavigate(); 
  const routeChange_login = () =>{ 
    let path_login = `/login`; 
    navigate(path_login);
  }
  let navigate_regis = useNavigate(); 
  const routeChange_regis = () =>{ 
    let path_regis = `/register`; 
    navigate_regis(path_regis);
  }
  

  return (
    <AppBar position="static">
        <Container maxWidth="xl">
            <Toolbar disableGutters>
                        <Typography
                            variant="h6"
                            sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}>
                            <img id='img_1' src="/airplane-around-earth.png" width="50 px" height="50 px"/>
                            <Link href="/" ><h3 id='h_1'>BookingResort</h3></Link>
                        </Typography>
                        &nbsp;&nbsp; 
                        <ButtonGroup color="secondary" variant="text" aria-label="text button group">
                        
                            <Button onClick={routeChange_login} >เข้าสู่ระบบ</Button>
                            <Button onClick={routeChange_regis}>สมัครสมาชิก</Button>
                           
                        </ButtonGroup>          
            </Toolbar>
        </Container>
    </AppBar>
  );
};
export default Header;




