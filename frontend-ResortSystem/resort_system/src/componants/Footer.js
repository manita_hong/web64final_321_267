
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';

function Footer() {
  return (
    <AppBar position="static" color="secondary" >
        <Container maxWidth="xl" >
            <Toolbar>
                        
                <Typography variant="subtitle2"   >
                      Created @By Manita Kaewpiyarat 
                </Typography>
                &nbsp;&nbsp;
            
            </Toolbar>
        </Container>
    </AppBar>
  );
};
export default Footer;

