import './Header.css';
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import Link from '@mui/material/Link';
import ButtonGroup from '@mui/material/ButtonGroup';
import { useNavigate } from "react-router-dom";
import Avatar from '@mui/material/Avatar';
import { deepOrange, deepPurple } from '@mui/material/colors';

function Header() {

    let navigate_home2= useNavigate(); 
  const routeChange_home2 = () =>{ 
    let path_login = `/home2`; 
    navigate_home2(path_login);
  }
  let navigate_logout = useNavigate(); 
  const routeChange_logout = () =>{ 
    let path_regis = `/`; 
    navigate_logout(path_regis);
  }
  let navigate_system = useNavigate(); 
  const routeChange_system = () =>{ 
    let path_system = `/system`; 
    navigate_system(path_system);
  }

  return (
    <AppBar position="static">
        <Container maxWidth="xl">
            <Toolbar disableGutters>
            
                    <Typography
                        variant="h6"
                        sx={{ mr: 2, display: { xs: 'none', md: 'flex' } }}>
                        <img id='img_1' src="/airplane-around-earth.png" width="50 px" height="50 px"/>
                        <Link href="/home2" ><h3 id='h_1'>BookingResort</h3></Link>
                    </Typography>
                        &nbsp;&nbsp; 
                    <ButtonGroup color="secondary" variant="text" aria-label="text button group" >
                        <Button onClick={routeChange_home2} >หน้าแรก</Button>
                        <Button onClick={routeChange_system}>จองรีสอร์ท</Button>
                    </ButtonGroup>         
                    <Button style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end' }}>
                       <Avatar sx={{ bgcolor: deepOrange[800] }}>H</Avatar>
                    </Button>
                    <Button 
                     variant="outlined" 
                     color="secondary"   
                     style={{ marginLeft: "auto" }} 
                     onClick={routeChange_logout} 
                     >
                        ออกจากระบบ    
                    </Button>
                       
                        
            </Toolbar>
        </Container>
    </AppBar>
  );
};
export default Header;
