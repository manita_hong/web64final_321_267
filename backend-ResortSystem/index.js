const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')

const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET =process.env.TOKEN_SECRET

const mysql = require('mysql');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'resort_admin',
    password: 'resort_admin',
    database: 'ResortSystem'
})

connection.connect()
const express = require('express')
const app = express()
const port = 4000


/*------------Middleware for Authenticating User Token-------------*/

function authenticateToken (req, res, next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if(err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}


/*------------------Add a new Person-----------------*/
app.post("/add_person", (req, res) => {
    
    let person_name = req.query.person_name
    let person_surname = req.query.person_surname
    let person_username = req.query.person_username
    let person_password = req.query.person_password
    
    bcrypt.hash(person_password, SALT_ROUNDS, (err,hash) => {

        let query = `INSERT INTO BookingInformation
                    (BookingName, BookingSurname, Username, Password)
                    VALUES ('${person_name}','${person_surname}',
                            '${person_username}','${hash}'
                    )`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Adding new user succesful"
              })
        }
    })
  })

})


/*--------------------Update Person--------------------*/
app.post("/update_person", (req, res) => {
    
    let person_id = req.query.person_id
    let person_name = req.query.person_name
    let person_surname = req.query.person_surname
    let person_username = req.query.person_username

    let query = `UPDATE BookingInformation SET
                    BookingName='${person_name}',
                    BookingSurname='${person_surname}',
                    Username='${person_username}'
                    WHERE BookingID=${person_id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error updating record"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Updating person succesful"
              })
        }
    })
})


/*-----------------------Delete Person-----------------------*/
app.post("/delete_person", (req, res) => {
    
    let person_id = req.query.person_id

    let query = `DELETE FROM BookingInformation 
                    WHERE BookingID=${person_id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error deleting record"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Deleting record succesful"
              })
        }
    })
})


/*-----------------------Add a new Resort----------------------*/
app.post("/add_resort", (req, res) => {
    let resort_name = req.query.resort_name
    let resort_location = req.query.resort_location

    let query = `INSERT INTO Resort
                    (ResortName, ResortLocation)
                    VALUES ('${resort_name}','${resort_location}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Adding resort succesful"
              })
        }
    })
})

/*-----------------------Update ResortLocation----------------------*/
app.post("/update_resort", (req, res) => {
    
    let resort_id = req.query.resort_id
    let resort_name = req.query.resort_name
    let resort_location = req.query.resort_location

    let query = `UPDATE Resort SET
                    ResortName='${resort_name}',
                    ResortLocation='${resort_location}'
                    WHERE ResortID=${resort_id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error updating record"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Updating resort succesful"
              })
        }
    })
})

/*-----------------------Delete ResortLocation----------------------*/
app.post("/delete_resort", (req, res) => {
    
    let resort_id = req.query.resort_id

    let query = `DELETE FROM Resort 
                    WHERE ResortID=${resort_id}`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error deleting record"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Deleting record succesful"
              })
        }
    })
})

/*---------------------------List All Registrations----------------------------*/
app.get("/list_registrations",(req, res) => {
    let query = `
    
    SELECT BookingInformation.BookingID,  BookingInformation.BookingName, BookingInformation.BookingSurname, Resort.ResortName, 
    Registration.ResortLocation, Registration.RegistrationTime 

        FROM BookingInformation, Registration, Resort

        WHERE (Registration.BookingID = BookingInformation.BookingID) AND
         (Registration.ResortID = Resort.ResortID);

         `
    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error querying from resort db"
                    })
        }else{
            res.json(rows)
        }
    })
})

/*---------------------------Login Registrations----------------------------*/
app.post("/login", (req, res) => {
    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM BookingInformation WHERE Username='${username}'`
    
        connection.query( query, (err, rows) => {
            if(err){
                console.log(err)
                res.json({
                          "status" : "400",
                          "message" : "Error querying from booking db"
                        })
            }else{
                let db_password = rows[0].Password
                bcrypt.compare(user_password, db_password,(err, result) => {
                    if (result){
                        let payload = {
                            "username" : rows[0].Username,
                            "user_id" : rows[0].BookingID,
                           
                        }
                        console.log(payload)
                        let token = jwt.sign(payload, TOKEN_SECRET, {expiresIn : '1d'})
                        res.send(token)
                    }else {res.send("Invalid username / password")}
                })
            }
    })
}) 


/*---------------------API for Registering to a new Resort-----------------*/
app.post("/register_resort", authenticateToken, (req, res) => {
   
    let booking_id = req.user.user_id
    let resort_id = req.query.resort_id
    let resort_location = req.query.resort_location
    let reservednight = req.query.reservednight

    let query = `INSERT INTO Registration
                    (BookingID, ResortID,ResortLocation, ReservedNight, RegistrationTime)
                    VALUES ('${booking_id}',
                            '${resort_id}',
                            '${resort_location}',
                            '${reservednight}',
                           NOW() )`
    console.log(query)

    connection.query( query, (err, rows) => {
        console.log(err)
        if(err){
            res.json({
                      "status" : "400",
                      "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200",
                "message" : "Adding resort succesful"
              })
        }
    })
})



app.listen(port, () => {
    console.log(`Now starting Resort System Backend ${port}`)
})

